#!/usr/bin/env ruby
total = 0
hits = {}
current_x = 0
current_y = 0
hits["#{current_x}:#{current_y}"] = true
ARGF.each do |line|
    line.chomp.split('').each do |move|
        if move == '>'
            current_x += 1
        elsif move == '<'
            current_x -= 1
        elsif move == '^'
            current_y += 1
        elsif move == 'v'
            current_y -= 1
        end
        hits["#{current_x}:#{current_y}"] = true
    end
end
puts hits.length

