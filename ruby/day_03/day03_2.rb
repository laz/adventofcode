#!/usr/bin/env ruby
total = 0
hits = {}
current_x = [0, 0]
current_y = [0, 0]
count = 0
hits["#{current_x[0]}:#{current_y[0]}"] = true
ARGF.each do |line|
    line.chomp.split('').each do |move|
        santa_or_robo = count % 2
        count += 1
        if move == '>'
            current_x[santa_or_robo] += 1
        elsif move == '<'
            current_x[santa_or_robo] -= 1
        elsif move == '^'
            current_y[santa_or_robo] += 1
        elsif move == 'v'
            current_y[santa_or_robo] -= 1
        end
        hits["#{current_x[santa_or_robo]}:#{current_y[santa_or_robo]}"] = true
    end
end
puts hits.length

