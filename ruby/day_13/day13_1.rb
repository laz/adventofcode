#!/usr/bin/env ruby

happiness = Hash.new
ARGF.read.scan(/([^ ]+) [^ ]+ ([^ ]+) ([^ ]+) .+ ([^.]+).\n/).each do |subject, operation, amount, target|
    happiness[[subject, target]] = (operation == 'gain') ? amount.to_i : 0 - amount.to_i
end

happiness_calculations = Hash.new
happiness.keys.flatten.uniq.permutation.to_a.each do |table|
    total = 0
    subject = table[0]
    table[1..-1].each do |target|
        total = total + happiness[[subject, target]]
        total = total + happiness[[target, subject]]
        subject = target
    end
    total = total + happiness[[table[0], table[-1]]]
    total = total + happiness[[table[-1], table[0]]]
    happiness_calculations[table] = total
end
puts happiness_calculations.max_by { |k,v| v }[1]
