#!/usr/bin/env ruby

store = {
    :Weapons => {
        :Dagger =>     { :Cost =>  8, :Damage => 4, :Armor => 0 },
        :Shortsword => { :Cost => 10, :Damage => 5, :Armor => 0 },
        :Warhammer =>  { :Cost => 25, :Damage => 6, :Armor => 0 },
        :Longsword =>  { :Cost => 40, :Damage => 7, :Armor => 0 },
        :Greataxe =>   { :Cost => 74, :Damage => 8, :Armor => 0 }
    },
    :Armor => {
        :None =>       { :Cost =>   0, :Damage => 0, :Armor => 0 },
        :Leather =>    { :Cost =>  13, :Damage => 0, :Armor => 1 },
        :Chainmail =>  { :Cost =>  31, :Damage => 0, :Armor => 2 },
        :Splintmail => { :Cost =>  53, :Damage => 0, :Armor => 3 },
        :Bandedmail => { :Cost =>  75, :Damage => 0, :Armor => 4 },
        :Platemail =>  { :Cost => 102, :Damage => 0, :Armor => 5 }
    },
    :Rings => {
        :None1 =>    { :Cost =>   0, :Damage => 0, :Armor => 0 },
        :None2 =>    { :Cost =>   0, :Damage => 0, :Armor => 0 },
        :Damage1 =>  { :Cost =>  25, :Damage => 1, :Armor => 0 },
        :Damage2 =>  { :Cost =>  50, :Damage => 2, :Armor => 0 },
        :Damage3 =>  { :Cost => 100, :Damage => 3, :Armor => 0 },
        :Defense1 => { :Cost =>  20, :Damage => 0, :Armor => 1 },
        :Defense2 => { :Cost =>  40, :Damage => 0, :Armor => 2 },
        :Defense3 => { :Cost =>  80, :Damage => 0, :Armor => 3 }
    }
}
player = {
    :Health => 100,
    :Damage => 0,
    :Armor => 0,
    :Name => 'player'
}
adversary = {
    :Health => 109,
    :Damage => 8,
    :Armor => 2,
    :Name => 'adversary'
}

wins = Hash.new
losses = Hash.new
store[:Weapons].each do |weapon_name, weapon|
    player[:Damage] += weapon[:Damage]

    store[:Armor].each do |armor_name, armor|
        player[:Armor] += armor[:Armor]

        store[:Rings].keys.dup.each do |ring1_name|
            ring1 = store[:Rings][ring1_name]
            player[:Damage] += ring1[:Damage]
            player[:Armor] += ring1[:Armor]

            store[:Rings].keys.dup.each do |ring2_name|
                next if ring2_name == ring1_name
                ring2 = store[:Rings][ring2_name]
                player[:Damage] += ring2[:Damage]
                player[:Armor] += ring2[:Armor]
                cost = weapon[:Cost] + armor[:Cost] + ring1[:Cost] + ring2[:Cost]

                lose_turns = ((player[:Health] * 1.0) / ([adversary[:Damage] - (armor[:Armor] + ring1[:Armor] + ring2[:Armor]), 1].max)).ceil
                win_turns = ((adversary[:Health] * 1.0) / ([weapon[:Damage] + ring1[:Damage] + ring2[:Damage] - adversary[:Armor], 1].max)).ceil
                value = [lose_turns, win_turns, cost, [weapon_name, weapon], [armor_name, armor], [ring1_name, ring1], [ring2_name, ring2]]
                wins[cost] = value if win_turns <= lose_turns
                losses[cost] = value if win_turns > lose_turns
            end
        end
    end
end
min = wins.keys.min
p [min, wins[min]]

max = losses.keys.max
p [max, losses[max]]

def simulate
    weapon = store[:Weapons][:Greataxe]
    armor = store[:Armor][:Platemail]
    ring1 = store[:Rings][:Defense3]
    ring2 = store[:Rings][:Damage3]

    player[:Damage] = weapon[:Damage] + ring1[:Damage] + ring2[:Damage]
    player[:Armor] = armor[:Armor] + ring1[:Armor] + ring2[:Armor]

    lose_turns = ((player[:Health] * 1.0) / ([adversary[:Damage] - (armor[:Armor] + ring1[:Armor] + ring2[:Armor]), 1].max)).ceil
    win_turns = ((adversary[:Health] * 1.0) / ([weapon[:Damage] + ring1[:Damage] + ring2[:Damage] - adversary[:Armor], 1].max)).ceil

    p [lose_turns, win_turns, weapon[:Cost] + armor[:Cost] + ring1[:Cost] + ring2[:Cost]]
    turn = 1
    until player[:Health] <= 0 || adversary[:Health] <= 0
        if turn % 2 == 1
            adversary[:Health] -= [player[:Damage] - adversary[:Armor], 1].max
        else
            player[:Health] -= [adversary[:Damage] - player[:Armor], 1].max
        end
        p [turn, (turn / 2.0).ceil, player, adversary]
        turn += 1
    end
end
