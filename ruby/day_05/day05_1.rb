#!/usr/bin/env ruby
total_nice = 0
ARGF.each do |line|
    line = line.chomp.downcase
    if line !~ /ab|cd|pq|xy/ && line =~ /([a-z])\1/ && line.scan(/[aeiou]/).size > 2
        total_nice += 1
    end
end
puts total_nice
