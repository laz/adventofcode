#!/usr/bin/env ruby
total_nice = 0
ARGF.each do |line|
    line = line.chomp.downcase
    if line =~ /([a-z][a-z]).*\1/ && line =~ /([a-z]).\1/
        total_nice += 1
    end
end
puts total_nice
