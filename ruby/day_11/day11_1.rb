#!/usr/bin/env ruby
password = ''
ARGF.each do |line|
    password = line.to_i(36).+(1).to_s(36)
end
sequences = Regexp.union([*?a..?z].each_cons(3).map(&:join))
until 
    sequences.match(password) &&
    password =~ /(.)\1/ &&
    password =~ /([^#{Regexp.last_match.captures[0]}])\1/  &&
    password !~ /[iol]/

    password = password.to_i(36).+(1).to_s(36).gsub('0', 'a')
    if password.length < 8
        password = 'aaaaaaaa'
    end
end
puts password
