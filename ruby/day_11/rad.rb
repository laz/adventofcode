#!/usr/bin/env ruby
s = ''
ARGF.each do |line|
    s = line.chomp.succ
end
r = Regexp.union [*?a..?z].each_cons(3).map(&:join)
s.succ! until s[r] && s !~ /[iol]/ && s.scan(/(.)\1/).uniq.size > 1
puts s
