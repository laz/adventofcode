#!/usr/bin/env ruby
floor = 0

ARGF.each do |line|
    line.split('').each do |move|
        if move == '('
            floor += 1
        elsif move == ')'
            floor -= 1
        end
    end
end
p floor
