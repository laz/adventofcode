#!/usr/bin/env ruby
floor = 0
basement = 0

ARGF.each do |line|
    line.split('').each.with_index do |move, iteration|
        if move == '('
            floor += 1
        elsif move == ')'
            floor -= 1
        end
        if floor < 0 && basement == 0
            basement = iteration + 1
        end
    end
end
p basement
