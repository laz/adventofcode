#!/usr/bin/env ruby
total = 0
ARGF.each do |line|
    height, width, length = line.chomp.split('x').map { |n| n.to_i }.sort
    total += (2 * height) + (2 * width) + (height * width * length)
end
puts total

