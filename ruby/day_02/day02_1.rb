#!/usr/bin/env ruby
total = 0
ARGF.each do |line|
    height, width, length = line.chomp.split('x').map { |n| n.to_i }.sort
    total += (3 * height * width) + (2 * width * length) + ( 2 * length * height)
end
puts total

