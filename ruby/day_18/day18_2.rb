#!/usr/bin/env ruby
mapping = { '#' => 1, '.' => 0, :on => '#', :off => '.' }
size = 99
next_grid = Array.new
previous_grid = Array.new
ARGF.each do |line|
    current = line.chomp.split('')
    next_grid.push(current.dup)
    previous_grid.push(current.dup)
end
1.upto(100).each do |iteration|
    previous_grid.each.with_index do |row, row_number|
        row.each.with_index do |element, column_number|
            next if (row_number == 0 || row_number == size) && (column_number == 0 || column_number == size)
            neighbors = []
            neighbors.push([row_number - 1, column_number - 1]) if row_number > 0 && column_number > 0
            neighbors.push([row_number - 1, column_number]) if row_number > 0
            neighbors.push([row_number - 1, column_number + 1]) if row_number > 0 && column_number < size
            neighbors.push([row_number, column_number - 1]) if column_number > 0
            neighbors.push([row_number, column_number + 1]) if column_number < size
            neighbors.push([row_number + 1, column_number - 1]) if row_number < size && column_number > 0
            neighbors.push([row_number + 1, column_number]) if row_number < size
            neighbors.push([row_number + 1, column_number + 1]) if row_number < size && column_number < size
            neighbors_on = neighbors.each.reduce(0) { |sum, pair| sum + mapping[previous_grid[pair.first][pair.last]] }
            if element == mapping[:on] && ![2, 3].include?(neighbors_on)
                next_grid[row_number][column_number] = mapping[:off]
            elsif element == mapping[:off] && neighbors_on == 3
                next_grid[row_number][column_number] = mapping[:on]
            end
        end
    end
    previous_grid = Array.new
    next_grid.each { |row| previous_grid.push(row.dup) }
end
puts next_grid.reduce(0) { |total, row| total + row.reduce(0) { |rtotal, element| rtotal + mapping[element] }}
