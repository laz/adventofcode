#!/usr/bin/env ruby

template = {
    :children => '3',
    :cats =>  '7',
    :samoyeds =>  '2',
    :pomeranians =>  '3',
    :akitas =>  '0',
    :vizslas =>  '0',
    :goldfish =>  '5',
    :trees =>  '3',
    :cars =>  '2',
    :perfumes =>  '1'
}.to_a

max = 0
final_sue = 0

ARGF.readlines.map do |entry|
    item = Hash.new
    entry.gsub(/[:,]/, '').split.each_slice(2) do |pair|
        item[pair[0].to_sym] = pair[1]
    end
    matches = [template & item.to_a][0]
    if matches.length > max
        max = matches.length
        final_sue = item[:Sue]
    end
end
puts final_sue
