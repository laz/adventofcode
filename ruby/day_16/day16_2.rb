#!/usr/bin/env ruby

template = {
    'children' => ->(p) { p == 3 },
    'cats' => ->(p) { p > 7 },
    'samoyeds' => ->(p) { p == 2 },
    'pomeranians' => ->(p) { p < 3 },
    'akitas' => ->(p) { p == 0 },
    'vizslas' => ->(p) { p == 0 },
    'goldfish' => ->(p) { p < 5 },
    'trees' => ->(p) { p > 3 },
    'cars' => ->(p) { p == 2 },
    'perfumes' => ->(p) { p == 1 },
    'Sue' => ->(p) { false }
}


max = 0
final_sue = 0
sue = 0

ARGF.readlines.map do |entry|
    item = Hash.new
    matches = 0
    entry.gsub(/[:,]/, '').split.each_slice(2) do |pair|
        sue = pair[1] if pair[0] == 'Sue'
        matches += 1 if template[pair[0]].call(pair[1].to_i)
    end
    if matches > max
        max = matches
        final_sue = sue
    end
end
puts final_sue
