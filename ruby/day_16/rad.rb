#!/usr/bin/env ruby

data = { children: 3, cats: 7, samoyeds: 2, pomeranians: 3, akitas: 0, vizslas: 0, goldfish: 5, trees: 3, cars: 2, perfumes: 1 }
comp = { cats: :>, trees: :>, pomeranians: :<, goldfish: :< }
ARGF.each do |line|
  input = line.scan(/([a-z]+):\s(\d+)/).map{|m| [m.first.to_sym, m.last.to_i]}
  p line if input.delete_if{|m| ! m.last.send(comp[m.first] || :==, data[m.first])}.length == 3
end
