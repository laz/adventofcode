#!/usr/bin/env ruby
require 'json'

def rsum(val)
  return 0 if val.respond_to?(:has_value?) && val.has_value?("red")
  val.respond_to?(:each) ? val.each.reduce(0) {|sum, v| sum + rsum(v)} : val.to_i
end

p rsum(JSON.parse(ARGF.read)) 
