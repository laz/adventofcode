#!/usr/bin/env ruby
require 'json'
def summit(books)
    total = 0
    if books.is_a?(Enumerable)
        total = total + books.reduce(0) do |sum, element|
            if element.is_a?(Fixnum)
                sum = sum + element
            elsif element.is_a?(Enumerable)
                sum = sum + summit(element)
            end
            sum
        end
    end
    total
end

input = ''
ARGF.each do |line|
    input = line
end
the_books = JSON.parse(input, :symbolize_names => true)

puts summit(the_books)
