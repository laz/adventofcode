#!/usr/bin/env ruby

ingredients = File.readlines('input').map { |line| (line.scan /-?\d+/).map(&:to_i) } 
*properties, calories = ingredients.transpose 
ingredients_ratios = Array(1..99).permutation(ingredients.size).select do |ratios|  
  ratios.reduce(:+) == 100
end 
puts ingredients_ratios.map { |ingredient_ratio| 
  properties.map do |property| 
    properties_sum = property.zip(ingredient_ratio).reduce(0) { |sum, prop| sum + prop.reduce(:*) } 
    properties_sum < 0 ? 0 : properties_sum 
  end.reduce(:*) 
}.max  
