#!/usr/bin/env ruby

ingredients = Array.new
ARGF.read.scan(/([^ ]+): \D+ (-?\d+), \D+ (-?\d+), \D+ (-?\d+), \D+ (-?\d+), .*\n/).each do |ingredient|
    ingredients.push(ingredient.drop(1).map { |element| element.to_i })
end

num_props = ingredients.first.length
max = 1
0.upto(100) do |a|
    0.upto(100 - a) do |b|
        0.upto(100 - a - b) do |c|
            d = 100 - a - b - c
            props_scores = Array.new
            0.upto(num_props - 1) do |p|
                score = a * ingredients[0][p] + b * ingredients[1][p] + c * ingredients[2][p] + d * ingredients[3][p]
                props_scores.push(score) if score > 0
            end
            if props_scores.length == num_props
                current = props_scores.reduce(1, :*)
                max = current if current > max
            end
        end
    end
end
p max
