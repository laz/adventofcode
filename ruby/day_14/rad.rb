#!/usr/bin/env ruby

def parse line
  split_line = line.split " "
  speed = split_line[3].to_i
  sprint_time = split_line[6].to_i
  rest_time = split_line[-2].to_i

  progress = Array.new(sprint_time, speed) + Array.new(rest_time, 0)
  progress.cycle.first(2503).inject(:+)
end

puts ARGF.map{ |line| parse(line) }.max
