#!/usr/bin/env ruby

flight_length = 2503
distances = Hash.new
ARGF.read.scan(/([^ ]+) \D+ (\d+) \D+ (\d+) \D+ (\d+).*\n/).each do |deer, speed, duration, rest|
    duration = duration.to_i
    span = duration + rest.to_i
    total = flight_length / span
    speed = speed.to_i
    distances[deer] = (total * speed * duration) + [duration, flight_length % span].min * speed
end
puts distances.max_by { |k,v| v }[1]
