#!/usr/bin/env ruby

flight_length = 2503
distances = Hash.new
scores = Hash.new { 0 }

all_deer = Array.new
ARGF.read.scan(/([^ ]+) \D+ (\d+) \D+ (\d+) \D+ (\d+).*\n/).each do |current|
    all_deer.push(current)
end

1.upto(flight_length) do |current_second|
    all_deer.each do |deer, speed, duration, rest|
        duration = duration.to_i
        span = duration + rest.to_i
        total = current_second / span
        speed = speed.to_i
        distances[deer] = (total * speed * duration) + [duration, current_second % span].min * speed
    end
    maximum = distances.max_by { |k, v| v }[1]
    distances.select { |k, v| v == maximum }.keys.each do |lead_deer|
        scores[lead_deer] = scores[lead_deer].next
    end
end
p scores
puts scores.max_by { |k,v| v }[1]
