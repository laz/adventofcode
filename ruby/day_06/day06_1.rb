#!/usr/bin/env ruby
grid = Array.new(1000) { Array.new(1000) { 0 } }
ARGF.each do |line|
    command, start, ignore, finish = line.chomp.downcase.split(/(\d?\d?\d,\d?\d?\d)/)
    start_x, start_y = start.split(',').map { |n| n.to_i }
    finish_x, finish_y = finish.split(',').map { |n| n.to_i }

    for iteration_x in start_x..finish_x
        for iteration_y in start_y..finish_y
            if command == 'turn on '
                grid[iteration_x][iteration_y] = 1
            elsif command == 'turn off '
                grid[iteration_x][iteration_y] = 0
            elsif command == 'toggle '
                grid[iteration_x][iteration_y] = (1 - grid[iteration_x][iteration_y]).abs
            end
        end
    end
end
how_many_lit = grid.reduce(0) do |total, column|
    total + column.reduce(0) do |inner_total, lit|
        inner_total + lit
    end
end
puts how_many_lit
