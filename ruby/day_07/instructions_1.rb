#!/usr/bin/env ruby
h = {}
h[:b] = 14146
h[:c] = 0
h[:d] = h[:b] >> 2
h[:e] = h[:b] >> 3
h[:f] = h[:b] >> 5
h[:g] = h[:e] | h[:f]
h[:h] = h[:e] & h[:f]
h[:i] = ~ h[:h]
h[:j] = h[:g] & h[:i]
h[:k] = h[:d] | h[:j]
h[:l] = h[:d] & h[:j]
h[:m] = ~ h[:l]
h[:n] = h[:k] & h[:m]
h[:o] = h[:b] | h[:n]
h[:p] = h[:b] & h[:n]
h[:q] = ~ h[:p]
h[:r] = h[:o] & h[:q]
h[:s] = 1 & h[:r]
h[:t] = h[:c] << 1
h[:u] = h[:t] | h[:s]
h[:v] = h[:b] >> 1
h[:w] = h[:s] << 15
h[:x] = h[:v] | h[:w]
h[:y] = h[:x] >> 2
h[:z] = h[:x] >> 3
h[:aa] = h[:x] >> 5
h[:ab] = h[:z] | h[:aa]
h[:ac] = h[:z] & h[:aa]
h[:ad] = ~ h[:ac]
h[:ae] = h[:ab] & h[:ad]
h[:af] = h[:y] | h[:ae]
h[:ag] = h[:y] & h[:ae]
h[:ah] = ~ h[:ag]
h[:ai] = h[:af] & h[:ah]
h[:aj] = h[:x] | h[:ai]
h[:ak] = h[:x] & h[:ai]
h[:al] = ~ h[:ak]
h[:am] = h[:aj] & h[:al]
h[:an] = 1 & h[:am]
h[:ao] = h[:u] << 1
h[:ap] = h[:ao] | h[:an]
h[:aq] = h[:x] >> 1
h[:ar] = h[:an] << 15
h[:as] = h[:aq] | h[:ar]
h[:at] = h[:as] >> 2
h[:au] = h[:as] >> 3
h[:av] = h[:as] >> 5
h[:aw] = h[:au] | h[:av]
h[:ax] = h[:au] & h[:av]
h[:ay] = ~ h[:ax]
h[:az] = h[:aw] & h[:ay]
h[:ba] = h[:at] | h[:az]
h[:bb] = h[:at] & h[:az]
h[:bc] = ~ h[:bb]
h[:bd] = h[:ba] & h[:bc]
h[:be] = h[:as] | h[:bd]
h[:bf] = h[:as] & h[:bd]
h[:bg] = ~ h[:bf]
h[:bh] = h[:be] & h[:bg]
h[:bi] = 1 & h[:bh]
h[:bj] = h[:ap] << 1
h[:bk] = h[:bj] | h[:bi]
h[:bl] = h[:as] >> 1
h[:bm] = h[:bi] << 15
h[:bn] = h[:bl] | h[:bm]
h[:bo] = h[:bn] >> 2
h[:bp] = h[:bn] >> 3
h[:bq] = h[:bn] >> 5
h[:br] = h[:bp] | h[:bq]
h[:bs] = h[:bp] & h[:bq]
h[:bt] = ~ h[:bs]
h[:bu] = h[:br] & h[:bt]
h[:bv] = h[:bo] | h[:bu]
h[:bw] = h[:bo] & h[:bu]
h[:bx] = ~ h[:bw]
h[:by] = h[:bv] & h[:bx]
h[:bz] = h[:bn] | h[:by]
h[:ca] = h[:bn] & h[:by]
h[:cb] = ~ h[:ca]
h[:cc] = h[:bz] & h[:cb]
h[:cd] = 1 & h[:cc]
h[:ce] = h[:bk] << 1
h[:cf] = h[:ce] | h[:cd]
h[:cg] = h[:bn] >> 1
h[:ch] = h[:cd] << 15
h[:ci] = h[:cg] | h[:ch]
h[:cj] = h[:ci] >> 2
h[:ck] = h[:ci] >> 3
h[:cl] = h[:ci] >> 5
h[:cm] = h[:ck] | h[:cl]
h[:cn] = h[:ck] & h[:cl]
h[:co] = ~ h[:cn]
h[:cp] = h[:cm] & h[:co]
h[:cq] = h[:cj] | h[:cp]
h[:cr] = h[:cj] & h[:cp]
h[:cs] = ~ h[:cr]
h[:ct] = h[:cq] & h[:cs]
h[:cu] = h[:ci] | h[:ct]
h[:cv] = h[:ci] & h[:ct]
h[:cw] = ~ h[:cv]
h[:cx] = h[:cu] & h[:cw]
h[:cy] = 1 & h[:cx]
h[:cz] = h[:cf] << 1
h[:da] = h[:cz] | h[:cy]
h[:db] = h[:ci] >> 1
h[:dc] = h[:cy] << 15
h[:dd] = h[:db] | h[:dc]
h[:de] = h[:dd] >> 2
h[:df] = h[:dd] >> 3
h[:dg] = h[:dd] >> 5
h[:dh] = h[:df] | h[:dg]
h[:di] = h[:df] & h[:dg]
h[:dj] = ~ h[:di]
h[:dk] = h[:dh] & h[:dj]
h[:dl] = h[:de] | h[:dk]
h[:dm] = h[:de] & h[:dk]
h[:dn] = ~ h[:dm]
h[:do] = h[:dl] & h[:dn]
h[:dp] = h[:dd] | h[:do]
h[:dq] = h[:dd] & h[:do]
h[:dr] = ~ h[:dq]
h[:ds] = h[:dp] & h[:dr]
h[:dt] = 1 & h[:ds]
h[:du] = h[:da] << 1
h[:dv] = h[:du] | h[:dt]
h[:dw] = h[:dd] >> 1
h[:dx] = h[:dt] << 15
h[:dy] = h[:dw] | h[:dx]
h[:dz] = h[:dy] >> 2
h[:ea] = h[:dy] >> 3
h[:eb] = h[:dy] >> 5
h[:ec] = h[:ea] | h[:eb]
h[:ed] = h[:ea] & h[:eb]
h[:ee] = ~ h[:ed]
h[:ef] = h[:ec] & h[:ee]
h[:eg] = h[:dz] | h[:ef]
h[:eh] = h[:dz] & h[:ef]
h[:ei] = ~ h[:eh]
h[:ej] = h[:eg] & h[:ei]
h[:ek] = h[:dy] | h[:ej]
h[:el] = h[:dy] & h[:ej]
h[:em] = ~ h[:el]
h[:en] = h[:ek] & h[:em]
h[:eo] = 1 & h[:en]
h[:ep] = h[:dv] << 1
h[:eq] = h[:ep] | h[:eo]
h[:er] = h[:dy] >> 1
h[:es] = h[:eo] << 15
h[:et] = h[:er] | h[:es]
h[:eu] = h[:et] >> 2
h[:ev] = h[:et] >> 3
h[:ew] = h[:et] >> 5
h[:ex] = h[:ev] | h[:ew]
h[:ey] = h[:ev] & h[:ew]
h[:ez] = ~ h[:ey]
h[:fa] = h[:ex] & h[:ez]
h[:fb] = h[:eu] | h[:fa]
h[:fc] = h[:eu] & h[:fa]
h[:fd] = ~ h[:fc]
h[:fe] = h[:fb] & h[:fd]
h[:ff] = h[:et] | h[:fe]
h[:fg] = h[:et] & h[:fe]
h[:fh] = ~ h[:fg]
h[:fi] = h[:ff] & h[:fh]
h[:fj] = 1 & h[:fi]
h[:fk] = h[:eq] << 1
h[:fl] = h[:fk] | h[:fj]
h[:fm] = h[:et] >> 1
h[:fn] = h[:fj] << 15
h[:fo] = h[:fm] | h[:fn]
h[:fp] = h[:fo] >> 2
h[:fq] = h[:fo] >> 3
h[:fr] = h[:fo] >> 5
h[:fs] = h[:fq] | h[:fr]
h[:ft] = h[:fq] & h[:fr]
h[:fu] = ~ h[:ft]
h[:fv] = h[:fs] & h[:fu]
h[:fw] = h[:fp] | h[:fv]
h[:fx] = h[:fp] & h[:fv]
h[:fy] = ~ h[:fx]
h[:fz] = h[:fw] & h[:fy]
h[:ga] = h[:fo] | h[:fz]
h[:gb] = h[:fo] & h[:fz]
h[:gc] = ~ h[:gb]
h[:gd] = h[:ga] & h[:gc]
h[:ge] = 1 & h[:gd]
h[:gf] = h[:fl] << 1
h[:gg] = h[:gf] | h[:ge]
h[:gh] = h[:fo] >> 1
h[:gi] = h[:ge] << 15
h[:gj] = h[:gh] | h[:gi]
h[:gk] = h[:gj] >> 2
h[:gl] = h[:gj] >> 3
h[:gm] = h[:gj] >> 5
h[:gn] = h[:gl] | h[:gm]
h[:go] = h[:gl] & h[:gm]
h[:gp] = ~ h[:go]
h[:gq] = h[:gn] & h[:gp]
h[:gr] = h[:gk] | h[:gq]
h[:gs] = h[:gk] & h[:gq]
h[:gt] = ~ h[:gs]
h[:gu] = h[:gr] & h[:gt]
h[:gv] = h[:gj] | h[:gu]
h[:gw] = h[:gj] & h[:gu]
h[:gx] = ~ h[:gw]
h[:gy] = h[:gv] & h[:gx]
h[:gz] = 1 & h[:gy]
h[:ha] = h[:gg] << 1
h[:hb] = h[:ha] | h[:gz]
h[:hc] = h[:gj] >> 1
h[:hd] = h[:gz] << 15
h[:he] = h[:hc] | h[:hd]
h[:hf] = h[:he] >> 2
h[:hg] = h[:he] >> 3
h[:hh] = h[:he] >> 5
h[:hi] = h[:hg] | h[:hh]
h[:hj] = h[:hg] & h[:hh]
h[:hk] = ~ h[:hj]
h[:hl] = h[:hi] & h[:hk]
h[:hm] = h[:hf] | h[:hl]
h[:hn] = h[:hf] & h[:hl]
h[:ho] = ~ h[:hn]
h[:hp] = h[:hm] & h[:ho]
h[:hq] = h[:he] | h[:hp]
h[:hr] = h[:he] & h[:hp]
h[:hs] = ~ h[:hr]
h[:ht] = h[:hq] & h[:hs]
h[:hu] = 1 & h[:ht]
h[:hv] = h[:hb] << 1
h[:hw] = h[:hv] | h[:hu]
h[:hx] = h[:he] >> 1
h[:hy] = h[:hu] << 15
h[:hz] = h[:hx] | h[:hy]
h[:ia] = h[:hz] >> 2
h[:ib] = h[:hz] >> 3
h[:ic] = h[:hz] >> 5
h[:id] = h[:ib] | h[:ic]
h[:ie] = h[:ib] & h[:ic]
h[:if] = ~ h[:ie]
h[:ig] = h[:id] & h[:if]
h[:ih] = h[:ia] | h[:ig]
h[:ii] = h[:ia] & h[:ig]
h[:ij] = ~ h[:ii]
h[:ik] = h[:ih] & h[:ij]
h[:il] = h[:hz] | h[:ik]
h[:im] = h[:hz] & h[:ik]
h[:in] = ~ h[:im]
h[:io] = h[:il] & h[:in]
h[:ip] = 1 & h[:io]
h[:iq] = h[:hw] << 1
h[:ir] = h[:iq] | h[:ip]
h[:is] = h[:hz] >> 1
h[:it] = h[:ip] << 15
h[:iu] = h[:is] | h[:it]
h[:iv] = h[:iu] >> 2
h[:iw] = h[:iu] >> 3
h[:ix] = h[:iu] >> 5
h[:iy] = h[:iw] | h[:ix]
h[:iz] = h[:iw] & h[:ix]
h[:ja] = ~ h[:iz]
h[:jb] = h[:iy] & h[:ja]
h[:jc] = h[:iv] | h[:jb]
h[:jd] = h[:iv] & h[:jb]
h[:je] = ~ h[:jd]
h[:jf] = h[:jc] & h[:je]
h[:jg] = h[:iu] | h[:jf]
h[:jh] = h[:iu] & h[:jf]
h[:ji] = ~ h[:jh]
h[:jj] = h[:jg] & h[:ji]
h[:jk] = 1 & h[:jj]
h[:jl] = h[:ir] << 1
h[:jm] = h[:jl] | h[:jk]
h[:jn] = h[:iu] >> 1
h[:jo] = h[:jk] << 15
h[:jp] = h[:jn] | h[:jo]
h[:jq] = h[:jp] >> 2
h[:jr] = h[:jp] >> 3
h[:js] = h[:jp] >> 5
h[:jt] = h[:jr] | h[:js]
h[:ju] = h[:jr] & h[:js]
h[:jv] = ~ h[:ju]
h[:jw] = h[:jt] & h[:jv]
h[:jx] = h[:jq] | h[:jw]
h[:jy] = h[:jq] & h[:jw]
h[:jz] = ~ h[:jy]
h[:ka] = h[:jx] & h[:jz]
h[:kb] = h[:jp] | h[:ka]
h[:kc] = h[:jp] & h[:ka]
h[:kd] = ~ h[:kc]
h[:ke] = h[:kb] & h[:kd]
h[:kf] = 1 & h[:ke]
h[:kg] = h[:jm] << 1
h[:kh] = h[:kg] | h[:kf]
h[:ki] = h[:jp] >> 1
h[:kj] = h[:kf] << 15
h[:kk] = h[:ki] | h[:kj]
h[:kl] = h[:kk] >> 2
h[:km] = h[:kk] >> 3
h[:kn] = h[:kk] >> 5
h[:ko] = h[:km] | h[:kn]
h[:kp] = h[:km] & h[:kn]
h[:kq] = ~ h[:kp]
h[:kr] = h[:ko] & h[:kq]
h[:ks] = h[:kl] | h[:kr]
h[:kt] = h[:kl] & h[:kr]
h[:ku] = ~ h[:kt]
h[:kv] = h[:ks] & h[:ku]
h[:kw] = h[:kk] | h[:kv]
h[:kx] = h[:kk] & h[:kv]
h[:ky] = ~ h[:kx]
h[:kz] = h[:kw] & h[:ky]
h[:la] = 1 & h[:kz]
h[:lb] = h[:kh] << 1
h[:lc] = h[:lb] | h[:la]
h[:ld] = h[:kk] >> 1
h[:le] = h[:la] << 15
h[:lf] = h[:ld] | h[:le]
h[:lg] = h[:lf] >> 2
h[:lh] = h[:lf] >> 3
h[:li] = h[:lf] >> 5
h[:lj] = h[:lh] | h[:li]
h[:lk] = h[:lh] & h[:li]
h[:ll] = ~ h[:lk]
h[:lm] = h[:lj] & h[:ll]
h[:ln] = h[:lg] | h[:lm]
h[:lo] = h[:lg] & h[:lm]
h[:lp] = ~ h[:lo]
h[:lq] = h[:ln] & h[:lp]
h[:lr] = h[:lf] | h[:lq]
h[:ls] = h[:lf] & h[:lq]
h[:lt] = ~ h[:ls]
h[:lu] = h[:lr] & h[:lt]
h[:lv] = 1 & h[:lu]
h[:lw] = h[:lc] << 1
h[:lx] = h[:lw] | h[:lv]
h[:ly] = h[:lf] >> 1
h[:lz] = h[:lv] << 15
h[:ma] = h[:ly] | h[:lz]
h[:a] = h[:lx]
puts h[:a]
