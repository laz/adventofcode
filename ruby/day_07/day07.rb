#!/usr/bin/env ruby
operations = { 'AND' => :&, 'OR' => :|, 'NOT' => :~, 'LSHIFT' => :<<, 'RSHIFT' => :>>}
operations_parser = '\b(' + operations.keys.join('|') + ')\b'
wires = Hash.new
gates = Hash.new { |hash, key| hash[key] = Array.new }

execute_gates = ->(wire, instructions = nil) do
    puts wire + ' -> ' + wires[wire].to_s
    if instructions == nil && wire =~ /[a-zA-Z]+/
        instructions =  gates[wire]
    end
    instructions.each do |instruction|
        if instruction.length > 0
            executor = instruction[0].to_s =~ /\d+/ ? instruction[0].to_i : wires[instruction[0]]
            if !executor.to_s.empty?
                target_wire = instruction[3]
                if instruction[1].to_s.empty?
                    wires[target_wire] = wires[wire]
                elsif instruction[2].to_s.empty?
                    wires[target_wire] = executor.send(instruction[1]).to_i
                else
                    argument = instruction[2].to_s =~ /\d+/ ? instruction[2].to_i : wires[instruction[2]]
                    if !argument.to_s.empty?
                        wires[target_wire] = executor.send(instruction[1], argument).to_i
                    end
                end
                target_value = wires[target_wire]
                if !target_value.to_s.empty? && target_value < 0
                    wires[target_wire] = 65535 + target_value + 1
                end
                execute_gates.call(target_wire)
            end
        end
    end
end

ARGF.each do |line|
    execute, target = line.chomp.split('->').map { |e| e.strip }
    left_hand, operation, right_hand = execute.split(/#{operations_parser}/).map { |e| e.strip }

    inputs = []
    if operation.to_s.empty? && execute =~ /\d+/
        wires[target] = execute.to_i
        inputs.push(target)
    else
        if operation.to_s.empty?
            first = execute
        else
            operation = operations[operation]
            first = right_hand =~ /\d+/ ? right_hand.to_i : right_hand
            if !left_hand.to_s.empty?
                third = first
                first = left_hand =~ /\d+/ ? left_hand.to_i : left_hand
            end
        end
        instruction = [first, operation, third, target]

        if first =~ /\d+/ && third =~ /\d+/
            execute_gates(first, [instruction])
        end
        if first =~ /[a-zA-Z]+/
            gates[first].push(instruction)
            inputs.push(first)
        end
        if third =~ /[a-zA-Z]+/
            gates[third].push(instruction)
            inputs.push(third)
        end
    end

    inputs.each { |input| execute_gates.call(input) }
end
puts wires['a']
