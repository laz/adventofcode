#!/bin/sh
# %s/^"\|"$//g
# %s/\\"/"/g
# %s/\\\\/2/g
# %s/\\x[0-9a-zA-Z]\{2\}/1/g
# %s/\S//ng

original_count=$(cat input | tr -d '\r\n' | wc -c)
processed_count=$(sed -E -e 's/^"|"$//g' input | sed -E -e 's/\\"/"/g' | sed -E -e 's/\\\\/2/g' | sed -E -e 's/\\x[0-9a-zA-Z]{2}/1/g' | tr -d '\r\n' | wc -c)
echo "$original_count - $processed_count" | bc
