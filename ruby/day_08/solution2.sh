#!/bin/sh
# %s/^"\|"$//g
# %s/\\/\\\\/g
# %s/"/\\"/g
# %s/^/"\\"/g
# %s/$/\\""/g
# %s/\S//ng

original_count=$(cat input | tr -d '\r\n' | wc -c)
processed_count=$(sed -E -e 's/^"|"$//g' input | sed -E -e 's/\\/\\\\/g' | sed -E -e 's/"/\\"/g' | sed -E -e 's/^/"\\"/g' | sed -E -e 's/$/\\""/g' | tr -d '\r\n' | wc -c)
echo "$processed_count - $original_count" | bc

