#!/usr/bin/env ruby

input = ARGV[0].to_i
iterations = input / 11
houses = Array.new(iterations + 1) { 0 }
lowest = -1

(1..iterations).each do |iteration|
    (iteration..iterations).step(iteration) do |house|
        houses[house] += (iteration * 11)
        break if house / 50 == iteration
    end
end
puts houses.index { |value| value >= input }
