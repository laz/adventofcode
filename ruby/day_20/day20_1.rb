#!/usr/bin/env ruby

input = ARGV[0].to_i
iterations = input / 10
houses = Array.new(iterations + 1) { 0 }
lowest = -1

(1..iterations).each do |iteration|
    (iteration..iterations).step(iteration) do |house|
        houses[house] += (iteration * 10)
    end
end
puts houses.index { |value| value >= input }
