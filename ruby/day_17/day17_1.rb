#!/usr/bin/env ruby
containers = []
fit = 150
matches = []
ARGF.each do |line|
    containers.push(line.chomp.to_i)
end
1.upto(containers.length).each do |number|
    containers.combination(number).to_a.each do |test|
        sum = test.reduce(0, :+)
        if sum == fit
            matches.push(test)
        end
    end
end
puts matches.length
