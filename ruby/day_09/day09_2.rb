#!/usr/bin/env ruby
distances = Hash.new
ARGF.each do |line|
    source, to, destination, equals, distance = line.chomp.split
    distances[[source, destination].sort] = distance.to_i
end
route_distances = Hash.new
distances.keys.flatten.uniq.permutation.to_a.each do |route|
    total = 0
    source = route[0]
    route[1..-1].each do |destination|
        total = total + distances[[source, destination].sort]
        source = destination
    end
    route_distances[route] = total
end
puts route_distances.max_by { |k,v| v }[1]
