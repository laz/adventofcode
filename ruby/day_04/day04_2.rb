#!/usr/bin/env ruby
require 'digest'

input = 'yzbqklnj'
test = 0

digest = Digest::MD5.hexdigest(input + test.to_s)
until digest.start_with?('000000')
    test += 1
    digest = Digest::MD5.hexdigest(input + test.to_s)
end
puts test

