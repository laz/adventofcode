#!/usr/bin/env ruby
input = ARGF.readlines

mappings = Hash.new { |hash, key| hash[key] = Array.new }
medicine = input.pop.chomp
input.pop
max_molecule = 0

input.each do |current|
    current.chomp.scan(/(\S+) .. (\S+)/).each do |mapping|
        mappings[mapping.first].push(mapping.last)
        max_molecule = mapping.first.length if mapping.first.length > max_molecule
    end
end

calibrated = Array.new
medicine.split('').each.with_index do |current, iteration|
    calibrated.push([current, iteration])
    max_molecule.times do |time|
        test = current
        index = time + iteration + 1
        if index < medicine.length
            test = test + medicine[index]
            if mappings.has_key?(test)
                calibrated.push([test, iteration])
            end
        end
        end
end

output = Array.new
calibrated.each do |pair|
    molecule = pair.first
    if mappings.has_key?(molecule)
        index = pair.last
        mappings[molecule].each do |mapping|
            variation = medicine.dup
            variation[index..(index + molecule.length - 1)] = mapping
            output.push(variation)
        end
    end
end

p output.uniq.length
