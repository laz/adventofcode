#!/usr/bin/env ruby
input = ARGF.readlines

mappings = Hash.new
medicine = input.pop.chomp
input.pop

input.each do |current|
    current.chomp.reverse.scan(/(\S+) .. (\S+)/).each do |mapping|
        mappings[mapping.first] = mapping.last
    end
end
count = 0
subs = '(' + mappings.keys.join('|') + ')'
current = medicine.reverse
current.gsub!(/#{subs}/) { |match| count += 1; mappings[match] } until current.length == 1
puts count
