#!/usr/bin/env ruby
input = ''
ARGF.each do |line|
    input = line
end
output = ''
count = 1
current = ''
1.upto(50) do
    for iteration in 0.upto(input.length - 1)
        current = input[iteration]
        if input[iteration + 1] != current
            output << count.to_s
            output << current
            count = 1
        else
            count += 1
        end
    end
    input = output
    puts output.length
    output = ''
    count = 1
end
