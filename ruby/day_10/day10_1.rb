#!/usr/bin/env ruby
input = ''
ARGF.each do |line|
    input = line
end
output = ''
count = 0
current = ''
(0..39).each do
    input.split('').each do |value|
        if value != current
            output = output + count.to_s + current if count > 0
            count = 1
            current = value
        else
            count += 1
        end
    end
    output = output + count.to_s + current
    input = output
    puts output.length
    output = ''
    count = 0
end
